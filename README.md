# CRC32 for istreams

A program to calculate the CRC32 of the bytes in an std::istream. The provided
main calculates the CRC32 for the standard input:

    $ echo -n 123456789 | ./crc32
    cbf43926

The implementation is ultimately based in [this gist](https://gist.github.com/timepp/1f678e200d9e0f2a043a9ec6b3690635).

Otherwise I found the following resources useful:

- [This page](https://barrgroup.com/Embedded-Systems/How-To/CRC-Calculation-C-Code)
contains a good step by step explanation of the algorithm. NOTE! The bit order
is reversed compared to the implementation given here.
- I found [this Q&A](https://math.stackexchange.com/questions/66098/crc-computation) useful
to understand what's this whole business of dividing polynomials.
