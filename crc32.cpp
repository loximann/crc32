#include <cstdint>
#include <iostream>
#include <vector>

class CRC32 {
public:
    CRC32(size_t a_bufsize = 2 * 1024 * 1024) : bufsize{a_bufsize}, table{CRC32::make_table()} {}

    using crc_t = uint32_t;

    static std::vector<crc_t> make_table()
    {
        const uint32_t polynomial{0xEDB88320};
        std::vector<uint32_t> table;

        for (int i = 0; i < 256; ++i) {
            uint32_t c = i;

            for (uint8_t bit = 0; bit < 8; ++bit) {
                if (c & 1) {
                    c = (c >> 1) ^ polynomial;
                }
                else {
                    c = (c >> 1);
                }
            }
            table.push_back(c);
        }

        return table;
    }

    crc_t operator()(std::istream& is) const
    {
        crc_t crc{0xffffffffu};

        char buf[this->bufsize];
        while (is) {
            is.read(buf, this->bufsize);
            const auto readsize = is.gcount();
            for (auto c = buf; c < buf + readsize; ++c) {
                crc = (crc >> 8) ^ this->table[(crc ^ *c) & 0xff];
            }
        }

        return crc ^ 0xffffffffu;
    }

    const size_t bufsize;
    const std::vector<crc_t> table;
};

int main()
{
    std::cout << std::hex << CRC32{}(std::cin) << std::endl;
}
